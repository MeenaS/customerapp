import React, { Component } from "react";
import {StyleSheet,View,Text,TouchableWithoutFeedback} from 'react-native';
import { createAppContainer,createSwitchNavigator} from 'react-navigation';
import { createBottomTabNavigator,createMaterialTopTabNavigator} from 'react-navigation-tabs';
import { createStackNavigator,HeaderBackButton } from 'react-navigation-stack';
import Home from '../screens/home'
import Filter from '../screens/filter'
import Cart from '../screens/cart'
import Orders from '../screens/orders'
import Profile from '../screens/profile'
import PastOrders from '../screens/pastOrders'
import UpComing from '../screens/upComing'
import OrderInvoice from '../screens/orderInvoice'
import Menu from '../screens/menu'
import Favorites from '../screens/favorites'
import Payment from '../screens/payment'
import Login from '../screens/login'
import SignUp from '../screens/signUp'
import Address from '../screens/address'
import Restaurant from '../screens/restaurant'
import {Icon} from 'react-native-elements'
import RBSheet from 'react-native-raw-bottom-sheet'


const switchNavigator1=createSwitchNavigator({

Cart:{
screen:Cart,
}
})

const stackNavigator2=createStackNavigator({
OrderInvoice:{
screen:OrderInvoice,
navigationOptions: () => ({
      title: `Order Invoice`,
      headerStyle:{
      backgroundColor:'#ffd700'
      },
      headerLeft:(<Icon name="close" type="antdesign" iconStyle={{marginLeft:10}}/> ),
      headerRight:(<Icon name="question-circle-o" type="font-awesome" size={30} iconStyle={{marginRight:10}}/>)
      }),
}
})

const stackNavigator4=createStackNavigator({
Address:{
screen:Address,
  navigationOptions: ({ navigation }) => {
      return {
        headerLeft: (<HeaderBackButton onPress={_ => navigation.navigate("Profile")} />),
        title: 'Saved Addresses',
        headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
        headerStyle: {
        backgroundColor: '#ffd700',
        }
      }
    }
}
})

const stackNavigator5=createStackNavigator({
Restaurant:{
screen:Restaurant,
  navigationOptions: ({ navigation }) => {
      return {
        headerLeft: (<HeaderBackButton onPress={_ => navigation.navigate("Home")} />),
        title: 'Lorem Ipsum Restaurant',
        headerTitleStyle :{textAlign: 'center',alignSelf:'center'},
        headerStyle: {
        backgroundColor: '#ffd700',
        }
      }
    }
}
})

const stackNavigator3=createStackNavigator({
Menu:{
 screen:Menu,
 navigationOptions: () => ({
       title: `Lorem Ipsum Restaurant`,
       headerStyle:{
       backgroundColor:'#ffd700'
       },
       headerLeft:(<Icon name="arrowleft" type="antdesign" iconStyle={{marginLeft:10}}/> ),
       headerRight:(<Icon name="heart" type="antdesign" size={30} iconStyle={{marginRight:10,color:'red'}}/>)
       }),

 }
 })



 switchNavigator1.navigationOptions = ({ navigation }) => {

        let tabBarVisible = true;

        let routeName = navigation.state.routes[navigation.state.index].routeName

        if ( routeName == 'Cart' ) {
            tabBarVisible = false
        }

        return {
            tabBarVisible,
        }
    }

const topNavigator=createMaterialTopTabNavigator(
{
PastOrders:{
screen:PastOrders,
},
UpComing:{
screen:UpComing,
}

},
 {
       tabBarPosition: 'top',
       swipeEnabled: false,
       animationEnabled: true,
       tabBarOptions: {
       activeTintColor: 'black',
       inactiveTintColor: 'black',
       style: {
         backgroundColor: '#daa520',
       },
       labelStyle: {
         textAlign: 'center',
       },
       indicatorStyle: {
         borderBottomColor: 'black',
         borderBottomWidth: 3,
         marginLeft:13,
         width:'48%'

 },
 },
 }
)

const bottomNavigator = createBottomTabNavigator(
{
HomeScreen:{
screen:Home,
navigationOptions:{
tabBarIcon: ({ tintColor }) => (
<View>
<Icon style={[{color: tintColor}]} name="shop" size={25} type='entypo' />
</View>)
}
},
Filter: { screen: Filter,
navigationOptions:{
tabBarIcon: ({ tintColor }) => (
<View>
<Icon style={[{color: tintColor}]} size={25} name='filter-list' type='MaterialIcons'
onPress={() => {this.RBSheet.open();}} />
<View>
 <RBSheet
 ref={ref => {
 this.RBSheet = ref;
 }}
 height={400}
 duration={700}>
 <Filter />

 <TouchableWithoutFeedback onPress={() => {this.RBSheet.close();}}>
 <Text style={{color:'red',position: "absolute", bottom: 18, left: 40,fontSize:18}}>CANCEL</Text>
 </TouchableWithoutFeedback>
  <TouchableWithoutFeedback >
  <View>
  <Text style={{backgroundColor:'green',width:'35%',textAlign:'center',position: "absolute", bottom: -80, right: 40,fontSize:16,color:'white'}}>APPLY FILTER</Text>
  </View>
  </TouchableWithoutFeedback>

</RBSheet>
</View>
</View>
)
}
},
Cart:{
screen:switchNavigator1,
navigationOptions:{
tabBarIcon: ({ tintColor }) => (
<View>
<Icon style={[{color: tintColor}]} size={25} name='shopping-bag' type='feather'/>
</View>)
}
},
Orders: {
screen: topNavigator,
navigationOptions:{
tabBarIcon: ({ tintColor }) => (
<View>
<Icon style={[{color: tintColor}]} size={25} name='list-alt' type='font-awesome' />
</View>)
}
},
Profile: {
screen: Profile,
navigationOptions:{
tabBarIcon: ({ tintColor }) => (
<View>
<Icon style={[{color: tintColor}]} size={25} name='user' type='font-awesome' />
</View>)
}
},
}
)

const switchNavigator = createSwitchNavigator({
Login:Login,
SignUp:SignUp,
Home:bottomNavigator,
OrderInvoice:stackNavigator2,
Menu:stackNavigator3,
Favorites:Favorites,
Payment:Payment,
Address:stackNavigator4,
Restaurant:stackNavigator5
})

export default createAppContainer(switchNavigator);