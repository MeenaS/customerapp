import React, { Component } from "react";
import {StyleSheet, Text, View,TouchableWithoutFeedback} from 'react-native';
import CartData from '../data/cart.json';
import {Icon,Header} from 'react-native-elements';
import Dialog from "react-native-dialog";

class Cart extends Component{

state = {
    dialogVisible: false
  };

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };

  handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    this.setState({ dialogVisible: false });
  };

render()

{
return(
<View style={{flex:1}}>
<Header
  leftComponent={<Icon name="arrowleft" type="antdesign" onPress={() => this.props.navigation.goBack()} iconStyle={{marginBottom:20}}/> }
  centerComponent={{ text: 'Items in Cart', style: { color: 'black',fontSize:20,marginRight:80,marginBottom:20} }}
  rightComponent={<Text style={{marginBottom:20,fontSize:12}}>CLEAR CART</Text>}
  containerStyle={{
  backgroundColor: '#ffd700',
  height:'8%'
  }}
/>
{
CartData.map((details,index)=>
{
return(


<View style={{flexDirection:'row',justifyContent:'space-between',borderBottomWidth:2,borderBottomColor:'gray'}}>
<View style={{flexDirection:'row'}}>
<Text style={{marginTop:10,padding:20,color:'black',fontSize:20}}>{details.name}</Text>
<Icon name="square-inc" type="material-community" iconStyle={{color:'green',marginTop:30}}/>
</View>
<Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>Are you sure to remove this item from cart?</Dialog.Title>
          <Dialog.Button label="Cancel" onPress={this.handleCancel} style={{color:'red'}}/>
          <Dialog.Button label="OK" onPress={this.handleDelete} />
        </Dialog.Container>
<TouchableWithoutFeedback onPress={this.showDialog}>
<View style={{flexDirection:'column',marginRight:20}}>
<Text style={{fontSize:20,marginTop:10}}>{details.qty} * {details.price}</Text>
<View style={{flexDirection:'row'}}>
<Icon name="rupee" type="font-awesome" size={20} iconStyle={{marginTop:8,marginRight:6}}/>
<Text style={{fontSize:20,color:'green'}}>{details.total}</Text>
</View>
</View>
</TouchableWithoutFeedback>
</View>

);
}
)
}
<View style={{position: 'absolute',bottom: 90, right:10}}>
<Text style={{fontSize:20}}>SubTotal  xyz</Text>
<Text style={{fontSize:20,marginTop:10,marginRight:10}}>Delivery fee abx</Text>
<Text style={{fontSize:20,marginTop:10,marginRight:10}}>Order Total  zzz</Text>
</View>
<View style={styles.bottomView}>
<View style={{flexDirection:'row'}}>
<Text style={{color:'black',fontWeight:'bold',fontSize:16}}>PROCEED TO CHECKOUT</Text>
<Text style={{fontSize:18,color:'green',marginLeft:30,backgroundColor:'#ffe4c4',width:'30%',textAlign:'center'}}>XYZ.ab</Text>
</View>
</View>
</View>

);
}
}

export default Cart;

const styles=StyleSheet.create({
bottomView:{
width: '100%',
height: 50,
backgroundColor: '#ffc44d',
justifyContent: 'center',
alignItems: 'center',
position: 'absolute',
bottom: 0
},
})