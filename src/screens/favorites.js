import React, { Component } from "react";
import {StyleSheet, Text, View,Image} from 'react-native';
import {Card,Button,Icon,Header} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class Favorites extends Component{
render(){
return(
<View>
<Header
  leftComponent={<Icon name="arrowleft" type="antdesign" onPress={() => this.props.navigation.navigate('Profile')} iconStyle={{marginBottom:20}}/> }
  centerComponent={{ text: 'Favorites list', style: { color: 'black',fontSize:20,marginRight:80,marginBottom:20} }}

  containerStyle={{
  backgroundColor: '#ffd700',
  height:'12%'
  }}
/>
<View style={{flexDirection:'column'}}>
<Card>
<View style={{flexDirection:"row"}}>
<Image
style={styles.restaurantLogo}
source={require('../img/logo.jpg')}
/>
<View style={{flexDirection:"column",marginLeft:15}}>
<Text style={styles.itemName}>Lorem Ipsum Items name</Text>
<Text style={styles.itemName}>from restaurant name</Text>
<Icon name="delete" type="material-community" iconStyle={{color:'red',position:'absolute',bottom:0,right:-0}}/>
<View style={{alignItems:'flex-start',flexDirection:'row',marginTop:10}}>
<Icon name="thumbs-up" type="font-awesome" size={18}/>
<Icon name="star" type="antdesign" iconStyle={{marginLeft:20,color:'gold'}} size={18}/>
<Text style={{marginLeft:8}}>4.0/5</Text>
</View>
</View>
</View>
</Card>
<Card

  image={require('../img/logo.jpg')}>
  <View style={{flexDirection:'row'}}>
  <View style={{flexDirection:'column'}}>
  <Text>Lorem ipsum restaurant name</Text>
  <Text>Location</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name='star' type='antdesign' iconStyle={{color:'gold',marginLeft:15}} size={20} />
  <Text style={{marginLeft:8}}>4.0/5</Text>
  <Icon name="delete" type="material-community" iconStyle={{color:'red',marginLeft:50,marginTop:6}}/>
  </View>
  </View>
</Card>
</View>
</View>
);
}
}

export default Favorites;

const styles = StyleSheet.create({
restaurantLogo:{
width:wp('20%'),
height:hp('10%')
},
itemName:{
fontSize:hp(2.5),
color:'black'
}
});