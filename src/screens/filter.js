import React, { Component } from "react";
import {StyleSheet, Text, View,Switch} from 'react-native';
import {Icon} from 'react-native-elements';

class Filter extends Component{
constructor(props) {
  super(props);
  this.state = { SwitchValue: false };
}
render(){
return(
<View>
<Text style={{fontSize:20,fontWeight:'bold',color:'black',marginTop:20,marginLeft:15}}>Set Filter</Text>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>
<View style={{flexDirection:'row'}}>
<Text style={{marginLeft:15,marginTop:20}}>VEG only</Text>
<Icon name="square-inc" type="material-community" iconStyle={{color:'green',marginTop:18,marginLeft:5}} />
</View>
<Switch
      onValueChange={(value) => this.setState({SwitchValue: value})}
      value={this.state.SwitchValue} />
</View>

<Text style={styles.title}>Sort by Delivery Time</Text>
<Text style={styles.title}>Lorem Ipsum dolor</Text>
<Text style={styles.title}>Lorem Ipsum dolor</Text>
<Text style={styles.title}>Lorem Ipsum dolor</Text>
<Text style={styles.title}>Lorem Ipsum dolor</Text>
</View>

);
}
}

export default Filter;

const styles=StyleSheet.create({
title:{
borderBottomWidth:1,
color:'gray',
padding:10,
color:'black'
}
})