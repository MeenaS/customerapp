import React,{Component} from 'react';
import {StyleSheet,View,Alert,Text,Image,ScrollView,SafeAreaView,TouchableWithoutFeedback,ImageBackground,FlatList} from 'react-native';
import PostData from '../data/posts.json';
import {Card,Button,Icon} from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Modal from "react-native-modal";
import apiUrl from "../common/apiUrl";
import DeviceInfo from 'react-native-device-info';
const uniqueID = DeviceInfo.getUniqueID();

class Home extends Component{
   constructor(props){
        super(props);
        global.r_id='';
        this.state = {
            activeIndex:0,
            isModalVisible: false,
            carouselItems: [
            {
                title:"Item 1"
            },
            {
                title:"Item 2"
            },
            {
                title:"Item 3"
            },
            {
                title:"Item 4"
            },
            {
                title:"Item 5"
            }
        ],
        data: [],
        }
    }

    componentWillMount() {
    this.fetchData();
    }

    fetchData()
    {

    let collection={
    id:global.id,
    device_id:uniqueID
    }
    var url = apiUrl.HTTP+'/home';
    fetch(url,{
    method:'POST',
    body:JSON.stringify(collection),
    headers:new Headers({
    'Content-Type':'application/json',
    'Accept': 'application/json'
    })
    })
    .then((response) => response.json())
    .catch(error  => {
      console.log('There has been a problem with your fetch operation: '+ error.message);
    })


    .then(response=>{
    if(response.success==true)
    {
    this.setState({data:response.restaurant});
    }
    else
    Alert.alert('Bad response verify your credentials');
    })}

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
      };

    _renderItem({item,index}){
        return (
            <View style={{flex:1}}>
                <Image
                      style={styles.img}
                      source={require('../img/banner1.png')}
                    />
            </View>
        )
    }

    restaurant=(id)=>{
     global.r_id=id;
    this.props.navigation.navigate('Restaurant')
    }


renderEntries({ item, index }) {
return(
<View>
<TouchableWithoutFeedback onPress={()=>this.restaurant(item.restaurant_id)}  key={index}>
<Card>
<View style={{flexDirection:"row"}}>
<Image
style={styles.restaurantLogo}
source={require('../img/logo.jpg')}
/>
<View style={{flexDirection:"column",marginLeft:15}}>
<Text style={{fontSize:18,color:'black',fontWeight:'bold'}}>{item.restaurant_name}</Text>
<View style={{alignItems:'flex-start',flexDirection:'row',marginTop:10}}>
<Icon name="square-inc" type="material-community" iconStyle={{color:'green'}} size={20}/>
<Icon name="square-inc" type="material-community" iconStyle={{color:'red'}} size={20} />
<View style={{flexDirection:'row'}}>
<Icon name="clock" type="feather" iconStyle={{color:'black',marginLeft:10}} size={18}/>
<Text style={{marginLeft:6}}>20-30min</Text>
</View>
<View style={{flexDirection:'row'}}>
<Icon name='star' type='antdesign' iconStyle={{color:'gold',marginLeft:8}} size={20}/>
<Text>4.0/5</Text>
</View>
<Icon name="hearto" type="antdesign" iconStyle={{marginLeft:10}} size={18}/>
</View>
</View>
</View>
</Card>
</TouchableWithoutFeedback>
</View>

)
}


render() {
return (

<View>
<View style={{backgroundColor:'white',marginTop:8,width:'95%',height:'8%',marginLeft:10,borderWidth:1,borderColor:'black',borderRadius:10}}>
<View style={{flexDirection:'column',padding:12}}>
<Text>Delivery to</Text>
<Text>#21,Lorem Ipsum street,dolor road,consectur,..</Text>
<Icon name="edit-2" type="feather" iconStyle={{position:'absolute',top:-32,right:0}}/>

</View>
</View>

<ScrollView>
<Text style={styles.title}>Offers for you</Text>
<SafeAreaView>
         <View>

            <Carousel
                    autoplay={true}
                    ref={ref => this.carousel = ref}
                    data={this.state.carouselItems}
                    sliderWidth={wp('100%')}
                    itemWidth={wp('90%')}
                    renderItem={this._renderItem}
                    onSnapToItem = { index => this.setState({activeIndex:index}) }
                />
                </View>
</SafeAreaView>
<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10}}>
<Text style={{fontSize:18,color:'black',marginLeft:14}}>Restaurants Near You</Text>
<View style={{flexDirection:'row',marginRight:14}}>
<Text>VIEW ALL</Text>
<Icon name="greater-than" type="material-community" size={16} iconStyle={{marginTop:2}}/>
</View>
</View>

<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</ScrollView>
</View>
);
}

}

export default Home;

const styles = StyleSheet.create({

carouselContainer:{

},
restaurantLogo:{
width:wp('20%'),
height:hp('10%')
},
title:{
fontSize:hp(3),
color:'black',
fontWeight:'bold',
marginTop:10,
marginBottom:10,
marginLeft:10
},
img:{
borderRadius:15,
width:wp('90%'),
height:hp('22%')
}
});