import React, { Component } from "react";
import {StyleSheet, Text, View} from 'react-native';
import {Card,Icon} from 'react-native-elements';

class Menu extends Component{
render(){
return(
<View>
<Card

  image={require('../img/logo.jpg')} >
  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Icon name="square-inc" type="material-community" iconStyle={{color:'green'}}/>
  <Text>VEG</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name="square-inc" type="material-community" iconStyle={{color:'red'}}/>
  <Text>NON-VEG</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name="clockcircleo" type="antdesign" size={20}/>
  <Text> 20-30 MINS</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name='star' type='antdesign' iconStyle={{color:'gold'}} size={20}/>
  <Text style={{marginLeft:8}}>4.0/5</Text>
  </View>
  </View>
  <View style={{flexDirection:'row'}}>
  <Text>North Indian</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>South Indian</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Chines</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Tandoori</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Lorem Ipsum</Text>
  </View>

</Card>
<View style={{backgroundColor:'#dcdcdc',height:'10%',justifyContent:'center',marginTop:10}}>
  <Text style={{marginLeft:20}}>ALL TIME</Text>
  </View>
  <View style={{borderBottomWidth:1, color:'gray',padding:10}}>
  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Text style={{fontWeight:'bold'}}>Lorem Ipsum Item</Text>
  <Icon name="square-inc" type="material-community" iconStyle={{color:'green',marginLeft:10}} size={20}/>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name="rupee" type="font-awesome" size={18} iconStyle={{marginRight:6,marginTop:3}} />
  <Text style={{color:'green'}}>XY.ab</Text>
  </View>
  </View>
  <Text>Lorem ipsum item description</Text>
  </View>
  </View>
);
}
}

export default Menu;