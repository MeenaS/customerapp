import React, { Component } from "react";
import {StyleSheet, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class OrderInvoice extends Component{
render(){
return(
<View>
<View style={{margin:15}}>
<View style={styles.container}>
<Text style={styles.order}>Order #B12HDGFDD</Text>
<Text style={styles.delivered}>DELIVERED</Text>
</View>
<Text>Lorem Ipsum restaurant name</Text>
<Text>Location(area)</Text>
</View>
<Text style={{backgroundColor:'#dcdcdc',height:'8%'}}>ITMES</Text>
<View style={{borderBottomWidth:2,borderColor:'gray'}}>
<View style={{flexDirection:'row',margin:20,justifyContent:'space-between'}}>
<Icon name="thumbs-up" type="font-awesome" />
<View style={{flexDirection:'column'}}>
<Text style={{color:'black',fontWeight:'bold',fontSize:20}}>Lorem Ipsum Item name</Text>
<Text>Location</Text>
</View>
<View style={{flexDirection:'row'}}>
<Icon name="rupee" type="font-awesome" size={15}/>
<Text style={{color:'green',marginLeft:7}}>XYZ.ab</Text>
</View>
</View>
</View>
<View style={{borderBottomWidth:1,borderColor:'gray'}}>
<View style={{flexDirection:'row',justifyContent:'space-between',padding:15}}>
<View style={{flexDirection:'column'}}>
<Text style={{color:'gray'}}>Sub Total:</Text>
<Text style={{color:'gray'}}>Delivery Fee:</Text>
<Text style={{fontSize:20,color:'black',fontWeight:'bold'}}>Order Total:</Text>
</View>
<View style={{flexDirection:'column'}}>
<Text>XY.ab</Text>
<Text>a.y</Text>
<Text>XYZ.ab</Text>
</View>
<View style={{flexDirection:'column',marginTop:10}}>
<Text>Paid by cash</Text>
<Text style={{color:'green'}}>DOWNLOAD RECEIPT</Text>
</View>
</View>
</View>
<View style={{flexDirection:'row',borderBottomWidth:1,borderColor:'gray',padding:15}}>
<Text style={{color:'gray'}}>Delivery by  </Text>
<Text style={{color:'black', fontWeight:'bold',fontSize:16}}>Person name</Text>
<Icon name='star' type='antdesign' iconStyle={{color:'gold',marginLeft:120}} size={20}/>
<Text style={{marginLeft:10}}>4.3/5</Text>
</View>
</View>
);
}
}

export default OrderInvoice;

const styles=StyleSheet.create({
container:{
flexDirection:'row',
justifyContent:'space-between',
marginTop:10
},
order:{
color:'black',
fontWeight:'bold',
fontSize:20
},
delivered:{
borderWidth:1,
borderColor:'gray',
backgroundColor:'#dcdcdc',
height:hp('6%'),
width:wp('30%'),
textAlign:'center',
borderRadius:10
}

})