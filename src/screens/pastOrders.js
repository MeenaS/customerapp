import React, { Component } from "react";
import {StyleSheet, Text, View,Image,TouchableWithoutFeedback} from 'react-native';
import {Card,Icon} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class PastOrders extends Component{
render(){
return(
<Card

  image={require('../img/logo.jpg')} >
  <Text style={styles.restaurant_name}>Lorem Ipsum restaurant name</Text>

  <Text style={styles.location}>Location(area)</Text>
  <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Menu')}>
  <Text style={{position:'absolute',bottom:100,right:10,color:'white',fontWeight:'bold',fontSize:18}}>MENU</Text>
  </TouchableWithoutFeedback>
  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'column'}}>
  <Text>01 April 2019,10am</Text>
  <Text>Order B27675675</Text>
  <View style={{flexDirection:'row'}}>
  <Icon name='star' type='antdesign' iconStyle={{color:'gold'}} size={20}/>
  <Text style={{marginLeft:8}}>4.0/5</Text>
  </View>
  </View>
   <View style={{flexDirection:'row'}}>
    <TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('OrderInvoice')}>

  <Text style={{marginTop:20,color:'green'}}>
    DETAILS
  </Text>
  </TouchableWithoutFeedback>
  <Icon name="greater-than" type="material-community" size={15} iconStyle={{marginLeft:3,marginTop:22}}/>
  </View>
  </View>
</Card>

);
}
}

export default PastOrders;

const styles=StyleSheet.create({
restaurant_name:{
position:'absolute',
bottom:100,
color:'white',
fontSize:hp(3),
fontWeight:'bold',
},
location:{
position:'absolute',
bottom:85,
color:'white',
fontSize:hp(2)
}

})