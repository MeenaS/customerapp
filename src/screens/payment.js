import React, { Component } from "react";
import {StyleSheet, Text, View,Image} from 'react-native';
import {Icon,Card} from 'react-native-elements';
import ElevatedView from 'react-native-elevated-view'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class Payment extends Component{

render(){
return(
<View>
<View style={{flexDirection:'row',padding:15}}>
<Icon name="arrowleft" type="antdesign" size={24}  onPress={() => this.props.navigation.navigate('Profile')}/>
<Text style={styles.header}>Payment Methods</Text>
</View>
<Text style={styles.subTitle}>Set your default payment method</Text>
<ElevatedView
elevation={3}
style={styles.box}>
<View style={{flexDirection:'column'}}>
<View style={styles.paytm}>
<Text style={styles.letter1}>Pay</Text>
<Text style={styles.letter2}>tm</Text>
</View>
<Text style={styles.payOption}>Avenue</Text>
<Text style={styles.payOption}>Credit/Debit Card</Text>
<Text style={styles.payOption}>Cash</Text>
<Text style={styles.wallet}>Wallet</Text>
</View>
</ElevatedView>
<View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10}}>
<Text style={styles.savedOptions}>Saved Cards</Text>
<Text style={{marginRight:25,color:'green'}}>ADD NEW</Text>
</View>
<Card containerStyle={styles.cardDetails}>
<Text style={{fontSize:17}}>DEBIT</Text>
<Text style={{fontSize:18}}>**** **** **** 3208</Text>
<Icon name="delete" type="antdesign" iconStyle={{color:'red',position:'absolute',bottom:10,right:40}}/>
</Card>
</View>
);
}
}
export default Payment;

const styles=StyleSheet.create({
box:{
width:wp('86%'),
height:hp('42%'),
marginLeft:30
},
header:{
fontSize:hp(3),
fontWeight:'bold',
marginLeft:30
},
subTitle:{
margin:10,
marginLeft:30,
fontSize:hp(3)
},
paytm:{
flexDirection:'row',
borderBottomWidth:wp(.5),
padding:hp(2),
borderColor:'gray'
},
letter1:{
color:'#00008b',
fontWeight:'bold',
fontSize:hp(3.5)
},
letter2:{
color:'#00ced1',
fontWeight:'bold',
fontSize:hp(3.5)
},
payOption:{
fontSize:hp(3),
padding:hp(2),
borderBottomWidth:wp(.5),
borderColor:'gray'
},
wallet:{
fontSize:hp(3),
padding:hp(2),
},
savedOptions:{
marginLeft:25,
fontSize:hp(3),
fontWeight:'bold'
},
cardDetails:{
width:wp(86),
marginLeft:26
}
})