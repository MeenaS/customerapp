import React,{Component} from 'react';
import {StyleSheet,View,Text,Image,TouchableWithoutFeedback,Modal,Alert,TouchableOpacity,TextInput} from 'react-native';
import {Icon,Card} from 'react-native-elements';
import ElevatedView from 'react-native-elevated-view'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog from "react-native-dialog";


class Profile extends Component{

state = {
    dialogVisible: false
  };

  showDialog = () => {
    this.setState({ dialogVisible: true });
  };

  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };

  handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    this.setState({ dialogVisible: false });
  };

render(){
return(
<View>
<View style={{flexDirection:'row',justifyContent:'space-between',padding:40}}>
<Text style={styles.account}>Manage Your Account</Text>
<Icon  name='logout' type='simple-line-icon' />
</View>
<View style={styles.container}>
<ElevatedView
             elevation={3}
style={styles.stayElevated}>

<Image
source={require('../img/profile.jpg')}
style={styles.img}
/>
<View style={{flexDirection:'column'}}>
<Text style={styles.name}>Lorem Ipsum Name</Text>
<Text style={styles.details}>+91 9875456553</Text>
<Text style={styles.details}>someone@gmail.com</Text>

<View style={styles.tag}>
<TouchableWithoutFeedback>
<Text style={styles.edit}>Edit profile</Text>
</TouchableWithoutFeedback>

<Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>Change Password</Dialog.Title>
          <TextInput
          style={{borderBottomWidth: 1,
                          borderBottomColor: 'gray',}}
          placeholder="Current Password"
          />
          <TextInput
                    style={{borderBottomWidth: 1,
                                    borderBottomColor: 'gray',}}
                    placeholder="New Password"
                    />
                    <TextInput
                              style={{borderBottomWidth: 1,
                                              borderBottomColor: 'gray',}}
                              placeholder="Confirm new Password"
                              />
          <Dialog.Button label="Cancel" onPress={this.handleCancel} style={{color:'red'}}/>
          <Dialog.Button label="Save" onPress={this.handleDelete} />
        </Dialog.Container>

<TouchableWithoutFeedback onPress={this.showDialog}>
<Text style={styles.password}>Change password</Text></TouchableWithoutFeedback>
</View>
</View>
</ElevatedView>
<View style={{flexDirection:'row',justifyContent:'space-between'}}>
<ElevatedView
elevation={3}
style={styles.stayElevated1}>
<View style={{flexDirection:'row'}}>
<Icon size={50} name='gift' type='antdesign' iconStyle={{marginTop:16,marginLeft:15}}/>
<View style={{flexDirection:'column',marginLeft:20,marginTop:10}}>
<Text>Rewards</Text>
<Text style={{fontWeight:'bold',color:'black'}}>150</Text>
<Text>Coins</Text>
</View>
</View>
</ElevatedView>
<ElevatedView
elevation={3}
style={styles.stayElevated1}>
<View style={{flexDirection:'row'}}>
<Icon size={50} name='wallet' type='simple-line-icon' iconStyle={{marginTop:16,marginLeft:15}}/>
<View style={{flexDirection:'column',marginLeft:20,marginTop:10}}>
<Text>Wallet</Text>
<Text style={{fontWeight:'bold',color:'black'}}>23.22</Text>
<Text>INA</Text>
</View>
</View>
</ElevatedView>
</View>
<View style={{flexDirection:'row',alignItems:'center'}} >
<TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Favorites')} >
<View style={styles.fav}>
<Text>Favorites</Text>
<Icon  name='hearto' type='antdesign' iconStyle={{marginTop:10}}/>
</View>
</TouchableWithoutFeedback>
<TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Address')} >
<View style={styles.fav}>
<Text>Addresses</Text>
<Icon  name='location-pin' type='entypo' iconStyle={{marginTop:10}} size={30}/>
</View>
</TouchableWithoutFeedback>
<TouchableWithoutFeedback onPress={()=>this.props.navigation.navigate('Payment')} >
<View style={styles.fav}>
<Text>Payments</Text>
<Icon  name='md-paper' type='ionicon' iconStyle={{marginTop:10}} size={30}/>
</View>
</TouchableWithoutFeedback>
</View>
</View>
</View>
);
}
}

export default Profile;

const styles=StyleSheet.create({
account:{
fontSize:hp(3.5),
fontWeight:'bold',
color:'black'
},
container: {
alignItems:'center'
  },

  stayElevated: {
    width: wp(88),
    height: hp(29.5),
    margin: 10,
    backgroundColor: 'white',
    alignItems:"center",
    borderRadius:10
  },
  img:{
  width: wp(25),
   height: hp(14),
   position:'absolute',

   top:-50,
   borderRadius: 100/ 2
  },
  name:{
  textAlign:'center',
  marginTop:55,
  fontSize:hp(2.5),
  color:'black',
  fontWeight:'bold'
  },
  details:{
  textAlign:'center',
  color:'black',
  fontWeight:'bold'
  },
  stayElevated1: {
      width: wp(40),
      height: hp(12),
      margin: 10,
      backgroundColor: 'white',
      borderRadius:15
    },

    tag:{
    flexDirection:'row',
    marginTop:hp(6.5),
    height:hp('6%')
    },
    edit:{
    borderTopWidth:2,
    borderRightWidth:1,
    fontSize:hp(2.2),
    width:wp('44%'),
    borderColor:'gray',
    textAlign:'center',
    padding:5

    },
    password:{
    borderTopWidth:2,
    borderLeftWidth:1,
    fontSize:hp(2.2),
    width:wp('44%'),
    borderColor:'gray',
    textAlign:'center',
    padding:5
    },
    fav:{
        flexDirection:'column',
        alignItems:'center',
        borderWidth:1,
        borderColor:'#E8E8E8',
        color:'black',
        width:wp('28.5%'),
        height:hp(10),
        textAlign:'center',
        marginTop:10,
        backgroundColor:"#F8F8F8"
},



});