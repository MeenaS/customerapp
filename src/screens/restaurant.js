import React, { Component } from "react";
import {StyleSheet, Text, View,FlatList} from 'react-native';
import {Card,Button,Icon} from 'react-native-elements';
import apiUrl from "../common/apiUrl";
import DeviceInfo from 'react-native-device-info';
const uniqueID = DeviceInfo.getUniqueID();

class Restaurant extends Component{

constructor(props){
        super(props);
        this.state = {
          data: []
        }
        }
componentWillMount() {
    this.fetchData();
    }



    fetchData()
    {
    let collection={
    id:global.id,
    device_id:uniqueID,
    restaurant_id:global.r_id
    }
    var url = apiUrl.HTTP+'/restaurant';
    fetch(url,{
    method:'POST',
    body:JSON.stringify(collection),
    headers:new Headers({
    'Content-Type':'application/json',
    'Accept': 'application/json'
    })
    })
    .then((response) => response.json())
    .catch(error  => {
      console.log('There has been a problem with your fetch operation: '+ error.message);
    })


    .then(response=>{
    if(response.success==true)
    {
    this.setState({data:response.restaurant});
    }
    else
    Alert.alert('Bad response verify your credentials');
    })}

renderEntries({ item, index }) {
return(
<View>
<Text>{item.cuisines_name}</Text>
</View>
)
}

render(){
return(
<View>
<Card
  image={require('../img/logo.jpg')} >
  <View style={{flexDirection:'row',justifyContent:'space-between'}}>
  <View style={{flexDirection:'row'}}>
  <Icon name="square-inc" type="material-community" iconStyle={{color:'green'}}/>
  <Text>VEG</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name="square-inc" type="material-community" iconStyle={{color:'red'}}/>
  <Text>NON-VEG</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name="clockcircleo" type="antdesign" size={20}/>
  <Text> 20-30 MINS</Text>
  </View>
  <View style={{flexDirection:'row'}}>
  <Icon name='star' type='antdesign' iconStyle={{color:'gold'}} size={20}/>
  <Text style={{marginLeft:8}}>4.0/5</Text>
  </View>
  </View>
  <View style={{flexDirection:'row'}}>
  <Text>North Indian</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>South Indian</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Chines</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Tandoori</Text>
  <Icon name='dot-single' type='entypo' iconStyle={{color:'gray'}} size={20}/>
  <Text>Lorem Ipsum</Text>
  </View>
  </Card>

<FlatList
data={this.state.data}
extraData={this.state}
keyExtractor={(value, index) => index.toString()}
renderItem= {this.renderEntries.bind(this)}
/>
</View>
);
}
}

export default Restaurant;